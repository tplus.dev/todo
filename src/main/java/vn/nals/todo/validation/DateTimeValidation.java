package vn.nals.todo.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static vn.nals.todo.util.DateTimeUtil.FORMAT;

public class DateTimeValidation implements ConstraintValidator<DateTime, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        try {
            FORMAT.parse(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
