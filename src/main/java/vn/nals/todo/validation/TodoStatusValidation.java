package vn.nals.todo.validation;


import vn.nals.todo.entity.Todo;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TodoStatusValidation implements ConstraintValidator<TodoStatus, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        try {
            Todo.Status.valueOf(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
