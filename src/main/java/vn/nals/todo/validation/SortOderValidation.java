package vn.nals.todo.validation;


import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SortOderValidation implements ConstraintValidator<SortOder, String> {

    private static final String ASC = "asc";
    private static final String DESC = "desc";

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (StringUtils.isBlank(s)) {
            return true;
        }

        return ASC.equalsIgnoreCase(s.trim()) || DESC.equalsIgnoreCase(s.trim());
    }
}
