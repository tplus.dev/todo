package vn.nals.todo.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = SortOderValidation.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SortOder {
    String message() default "only 'asc' or 'desc'";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}


