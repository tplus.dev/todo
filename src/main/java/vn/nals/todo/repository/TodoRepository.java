package vn.nals.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.nals.todo.entity.Todo;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {
}
