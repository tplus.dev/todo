package vn.nals.todo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.nals.todo.dto.CreateTodoDto;
import vn.nals.todo.dto.UpdateTodoDto;
import vn.nals.todo.entity.Todo;
import vn.nals.todo.exception.NalsBusinessException;
import vn.nals.todo.exception.NalsValidationException;
import vn.nals.todo.repository.TodoRepository;
import vn.nals.todo.util.DateTimeUtil;
import vn.nals.todo.util.ValidatorUtil;

import java.util.Optional;

import static java.util.Objects.isNull;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public Page<Todo> getWithPaging(Pageable pageable) {
        return todoRepository.findAll(pageable);
    }

    public Todo save(CreateTodoDto createTodoDto) {
        ValidatorUtil.validate(createTodoDto);
        Todo newTodo = new Todo();
        newTodo.setWorkName(createTodoDto.getWorkName());
        newTodo.setStartDate(DateTimeUtil.toLocalDateTime(createTodoDto.getStartDate()));
        newTodo.setEndDate(DateTimeUtil.toLocalDateTime(createTodoDto.getEndDate()));
        newTodo.setStatus(Todo.Status.PLANING);

        if (newTodo.getEndDate().isBefore(newTodo.getStartDate())) {
            throw new NalsBusinessException("end date cannot before start date");
        }

        return todoRepository.save(newTodo);
    }

    public Todo update(Long id, UpdateTodoDto updateTodoDto) {
        ValidatorUtil.validate(updateTodoDto);
        Todo existedTodo = getOne(id);
        if (isNull(existedTodo)) {
            throw new NalsValidationException("not found todo with id: " + id);
        }
        existedTodo.setWorkName(updateTodoDto.getWorkName());
        existedTodo.setStartDate(DateTimeUtil.toLocalDateTime(updateTodoDto.getStartDate()));
        existedTodo.setEndDate(DateTimeUtil.toLocalDateTime(updateTodoDto.getEndDate()));
        existedTodo.setStatus(Todo.Status.valueOf(updateTodoDto.getStatus()));
        if (existedTodo.getEndDate().isBefore(existedTodo.getStartDate())) {
            throw new NalsBusinessException("end date cannot before start date");
        }
        return todoRepository.saveAndFlush(existedTodo);
    }

    public void delete(Long id) {
        Todo existedTodo = getOne(id);
        if (isNull(existedTodo)) {
            throw new NalsBusinessException("no entity with id " + id + " exists");
        }
        todoRepository.deleteById(id);
    }

    public Todo getById(Long id) {
        return Optional.ofNullable(getOne(id))
                .orElseThrow(() -> new NalsValidationException("not found todo with id: " + id));
    }

    private Todo getOne(Long id) {
        return Optional.ofNullable(id)
                .flatMap(todoRepository::findById)
                .orElse(null);
    }
}
