package vn.nals.todo.util;

import lombok.experimental.UtilityClass;

import java.util.Optional;

@UtilityClass
public class AuditUtil {

    public static String getLatestServiceName() {
        StackWalker.StackFrame stackFrame = StackWalker.getInstance()
                .walk(frames -> frames.filter(frame -> {
                    String className = frame.getClassName();
                    return className.contains("vn.nals.todo") && className.contains("Service");
                }).findAny().orElse(null));

        return Optional.ofNullable(stackFrame).map(StackWalker.StackFrame::getClassName).orElse("System");
    }

}
