package vn.nals.todo.util;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.format.DateTimeFormatter.ofPattern;

@UtilityClass
public class DateTimeUtil {

    public static final DateTimeFormatter FORMAT = ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    public LocalDateTime toLocalDateTime(String value) {
        try {
            return LocalDateTime.parse(value, FORMAT);
        } catch (Exception e) {
            return null;
        }
    }

    public String toString(LocalDateTime localDateTime) {
        try {
            return localDateTime.format(FORMAT);
        } catch (Exception e) {
            return null;
        }
    }

}
