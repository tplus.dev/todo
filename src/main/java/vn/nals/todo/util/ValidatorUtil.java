package vn.nals.todo.util;

import lombok.experimental.UtilityClass;
import vn.nals.todo.exception.NalsValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.Set;

@UtilityClass
public class ValidatorUtil {

    public static <T> void validate(T object) {
        Set<ConstraintViolation<T>> violations = Validation.buildDefaultValidatorFactory()
                .getValidator()
                .validate(object);
        if (!violations.isEmpty()) {
            String message = violations.stream()
                    .findFirst()
                    .map(violation -> String.format("%s: %s", violation.getPropertyPath(), violation.getMessage()))
                    .get();
            throw new NalsValidationException(message);
        }
    }
}
