package vn.nals.todo.util;

import lombok.experimental.UtilityClass;
import vn.nals.todo.dto.PagingDto;

import java.util.List;
import java.util.Map;

@UtilityClass
public class ResponseUtil {

    public static <T> Map<String, Object> wrapWithPaging(List<T> data, PagingDto pagingDto) {
        return Map.of("paging", pagingDto, "data", data);
    }

}
