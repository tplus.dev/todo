package vn.nals.todo.exception;

public class NalsTechnicalException extends RuntimeException {
    public NalsTechnicalException() {
    }

    public NalsTechnicalException(String message) {
        super(message);
    }

    public NalsTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public NalsTechnicalException(Throwable cause) {
        super(cause);
    }

    public NalsTechnicalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}