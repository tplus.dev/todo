package vn.nals.todo.exception;

public class NalsValidationException extends RuntimeException {
    public NalsValidationException() {
    }

    public NalsValidationException(String message) {
        super(message);
    }

    public NalsValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public NalsValidationException(Throwable cause) {
        super(cause);
    }

    public NalsValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}