package vn.nals.todo.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestControllerAdvice
public class NalsExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(NalsExceptionHandler.class);
    private static final Map<Class<?>, HttpStatus> EXCEPTION_HANDLER = Map.of(
            NalsBusinessException.class, HttpStatus.UNPROCESSABLE_ENTITY,
            NalsTechnicalException.class, HttpStatus.INTERNAL_SERVER_ERROR,
            NalsValidationException.class, HttpStatus.BAD_REQUEST,
            BindException.class, HttpStatus.BAD_REQUEST
    );

    @ExceptionHandler(Exception.class)
    public ResponseEntity<NalsExceptionResponse> convertToHttpResponse(Exception exception, ServletWebRequest webRequest) {
        String requestUri = webRequest.getRequest().getRequestURI();
        String method = webRequest.getRequest().getMethod();
        String currentTime = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        String logTrace = String.format("%s %s at %s", method, requestUri, currentTime);

        Optional<Map.Entry<Class<?>, HttpStatus>> entry = EXCEPTION_HANDLER.entrySet().stream()
                .filter(e -> e.getKey().isInstance(exception))
                .findFirst();

        List<String> messages = List.of(exception.getMessage());
        if (exception instanceof BindException) {
            messages = getMessage((BindException) exception);
        }

        LOGGER.warn(logTrace, exception);
        NalsExceptionResponse body = new NalsExceptionResponse(messages, logTrace, exception.getCause());
        return entry.map(e -> new ResponseEntity<>(body, e.getValue()))
                .orElseGet(() -> new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR));
    }

    private List<String> getMessage(BindException exception) {
        List<String> messages = new ArrayList<>();
        exception.getFieldErrors().forEach(fieldError -> {
            String fieldName = fieldError.getField();
            String message = fieldError.getDefaultMessage();
            messages.add(String.format("%s: %s", fieldName, message));
        });
        return messages;
    }
}