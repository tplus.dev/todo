package vn.nals.todo.exception;

public class NalsBusinessException extends RuntimeException{
    public NalsBusinessException() {
    }

    public NalsBusinessException(String message) {
        super(message);
    }

    public NalsBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public NalsBusinessException(Throwable cause) {
        super(cause);
    }

    public NalsBusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}