package vn.nals.todo.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NalsExceptionResponse {
    private List<String> messages;
    private String logTrace;
    private Throwable throwable;
}