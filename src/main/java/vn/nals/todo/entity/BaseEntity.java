package vn.nals.todo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

import static vn.nals.todo.util.AuditUtil.getLatestServiceName;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity {

    @JsonIgnore
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @JsonIgnore
    @Column(name = "created_by")
    private String createdBy;

    @JsonIgnore
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @JsonIgnore
    @Column(name = "updated_by")
    private String updatedBy;

    @JsonIgnore
    @Column(name = "deleted")
    private Boolean deleted;

    @PrePersist
    private void prePersistAudit() {
        createdAt = LocalDateTime.now();
        createdBy = getLatestServiceName();
        updatedAt = LocalDateTime.now();
        updatedBy = getLatestServiceName();
        deleted = false;
    }

    @PreUpdate
    private void preUpdateAudit() {
        updatedAt = LocalDateTime.now();
        updatedBy = getLatestServiceName();
    }
}
