package vn.nals.todo.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter @Setter
@Entity
@Table(name = "todo")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE todo SET deleted = true WHERE id = ?", check = ResultCheckStyle.NONE)
public class Todo extends BaseEntity {

    @AllArgsConstructor
    @Getter
    public enum Status {
        PLANING(0), DOING(1), COMPLETED(2);
        private final Integer value;
    }

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "work_name")
    private String workName;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "status")
    private Status status;
}
