package vn.nals.todo.dto;

import lombok.Getter;
import lombok.Setter;
import vn.nals.todo.validation.DateTime;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class CreateTodoDto {
    @NotBlank
    private String workName;

    @NotBlank
    @DateTime
    private String startDate;

    @NotBlank
    @DateTime
    private String endDate;
}
