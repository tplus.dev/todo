package vn.nals.todo.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import vn.nals.todo.exception.NalsValidationException;
import vn.nals.todo.validation.SortOder;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.lang.reflect.Field;
import java.util.Optional;

@Getter
@Setter
public class PagingDto {

    @Min(0)
    private Integer pageIndex;

    @Min(1) @Max(100)
    private Integer pageSize;

    private Integer pageTotal;

    private String sortBy;

    @SortOder
    private String sortOrder;

    public Pageable toPageable(Class<?> aClass) {
        this.pageIndex = Optional.ofNullable(this.pageIndex).orElse(0);
        this.pageSize = Optional.ofNullable(this.pageSize).orElse(10);
        this.sortBy = Optional.ofNullable(this.sortBy).orElse("id");
        Sort.Direction sortOrder = getDirectionFromString(this.sortOrder);
        this.sortOrder = sortOrder.name();
        checkValidSort(this.sortBy, aClass);

        Sort.Order order = new Sort.Order(sortOrder, sortBy);
        return PageRequest.of(pageIndex, pageSize, Sort.by(order));
    }

    private void checkValidSort(String sortBy, Class<?> aClass) {
        for(Field f: aClass.getDeclaredFields()) {
            if (f.getName().equals(sortBy)) {
                return;
            }
        }
        throw new NalsValidationException(String.format("no field '%s' found for type '%s'", sortBy, aClass.getSimpleName()));
    }

    private Sort.Direction getDirectionFromString(String sortOrder) {
        return Optional.ofNullable(sortOrder)
                .map(order -> {
                    try {
                        return Sort.Direction.fromString(order.trim().toUpperCase());
                    } catch (Exception e) {
                        return null;
                    }
                }).orElse(Sort.Direction.ASC);
    }
}
