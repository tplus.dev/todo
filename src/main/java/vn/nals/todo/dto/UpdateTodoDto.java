package vn.nals.todo.dto;

import lombok.Getter;
import lombok.Setter;
import vn.nals.todo.validation.DateTime;
import vn.nals.todo.validation.TodoStatus;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class UpdateTodoDto {
    @NotBlank
    private String workName;

    @NotBlank
    @DateTime
    private String startDate;

    @NotBlank
    @DateTime
    private String endDate;

    @NotBlank
    @TodoStatus
    private String status;
}
