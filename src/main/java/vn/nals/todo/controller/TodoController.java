package vn.nals.todo.controller;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.nals.todo.dto.CreateTodoDto;
import vn.nals.todo.dto.PagingDto;
import vn.nals.todo.dto.UpdateTodoDto;
import vn.nals.todo.entity.Todo;
import vn.nals.todo.service.TodoService;
import vn.nals.todo.util.ResponseUtil;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("todo")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public ResponseEntity<Map<String, Object>> getAll(@Valid @ModelAttribute PagingDto pagingDto) {
        Page<Todo> todos = todoService.getWithPaging(pagingDto.toPageable(Todo.class));
        pagingDto.setPageTotal(todos.getTotalPages());
        return ResponseEntity.ok(ResponseUtil.wrapWithPaging(todos.getContent(), pagingDto));
    }

    @PostMapping
    public ResponseEntity<Todo> createTodo(@RequestBody CreateTodoDto createTodoDto) {
        return ResponseEntity.ok(todoService.save(createTodoDto));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Todo> getById(@PathVariable Long id) {
        return ResponseEntity.ok(todoService.getById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Todo> updateTodo(@PathVariable Long id, @RequestBody UpdateTodoDto updateTodoDto) {
        return ResponseEntity.ok(todoService.update(id, updateTodoDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTodo(@PathVariable("id") Long id) {
        todoService.delete(id);
        return ResponseEntity.ok().build();
    }
}
