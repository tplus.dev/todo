package vn.nals.todo.util;

import lombok.experimental.UtilityClass;

import java.lang.reflect.Method;

@UtilityClass
public class Helper {

    public static <T> Method toPublicMethod(Class<T> aClass, String methodName, Class<?>... paramTypes) throws NoSuchMethodException {
        Method method = aClass.getDeclaredMethod(methodName, paramTypes);
        method.setAccessible(true);
        return method;
    }

}
