package vn.nals.todo.util;

import com.google.code.beanmatchers.BeanMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import vn.nals.todo.dto.CreateTodoDto;
import vn.nals.todo.dto.PagingDto;
import vn.nals.todo.dto.UpdateTodoDto;
import vn.nals.todo.entity.Todo;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.stream.Stream;

import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.hamcrest.MatcherAssert.assertThat;

public class JavaBeanTest {

    @BeforeEach
    void setUp() {
        BeanMatchers.registerValueGenerator(() -> {
                    Random r = new Random();
                    return LocalDateTime.of(r.nextInt(2000),
                            r.nextInt(12) + 1,
                            r.nextInt(27) + 1,
                            r.nextInt(24),
                            r.nextInt(60),
                            r.nextInt(60),
                            r.nextInt(999999999)
                    );
                }, LocalDateTime.class);
    }


    static Stream<Class<?>> beanClassForGetterSetterTest() {
        return Stream.of(Todo.class,
                PagingDto.class,
                CreateTodoDto.class,
                UpdateTodoDto.class);
    }

    @ParameterizedTest
    @MethodSource("beanClassForGetterSetterTest")
    void shouldHaveValidGetterAndSetter(Class<?> beanClass) {
        assertThat(beanClass, hasValidGettersAndSetters());
    }
}
