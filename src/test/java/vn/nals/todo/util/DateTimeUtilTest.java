package vn.nals.todo.util;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class DateTimeUtilTest {

    @Test
    void testToLocalDateTime() {
        var dateTime = "2022-05-21T00:00:00";
        assertEquals(LocalDateTime.of(2022,5,21,0,0), DateTimeUtil.toLocalDateTime(dateTime));

        dateTime = "2022-05-21";
        assertNull(DateTimeUtil.toLocalDateTime(dateTime));
    }

    @Test
    void testToString() {
        var dateTime = LocalDateTime.of(2022,5,21,0,0);
        assertEquals("2022-05-21T00:00:00", DateTimeUtil.toString(dateTime));

        dateTime = null;
        assertNull(DateTimeUtil.toString(dateTime));
    }

}