package vn.nals.todo.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AuditUtilTest {

    @Test
    void testAuditGetLatestServiceName() {
        assertEquals("System", AuditUtil.getLatestServiceName());
        assertEquals("vn.nals.todo.util.AuditUtilTest$FakeService", FakeService.callAudit());
    }

    static class FakeService {
        public static String callAudit() {
            return AuditUtil.getLatestServiceName();
        }
    }
}