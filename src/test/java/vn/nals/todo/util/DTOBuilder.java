package vn.nals.todo.util;

import lombok.experimental.UtilityClass;
import vn.nals.todo.dto.CreateTodoDto;
import vn.nals.todo.dto.PagingDto;
import vn.nals.todo.dto.UpdateTodoDto;
import vn.nals.todo.entity.Todo;

import java.time.LocalDateTime;

@UtilityClass
public class DTOBuilder {

    public static Todo buildTodo() {
        var todo = new Todo();
        todo.setId(1L);
        todo.setWorkName("Write UT");
        todo.setStartDate(LocalDateTime.of(2022, 5, 19, 8, 0));
        todo.setEndDate(LocalDateTime.of(2022, 5, 21, 0, 0));
        todo.setStatus(Todo.Status.PLANING);
        return todo;
    }

    public static CreateTodoDto buildCreateTodoDto() {
        var createTodoDto = new CreateTodoDto();
        createTodoDto.setWorkName("Write UT");
        createTodoDto.setStartDate("2022-05-19T08:00:00");
        createTodoDto.setEndDate("2022-05-21T00:00:00");
        return createTodoDto;
    }

    public static UpdateTodoDto buildUpdateTodoDto() {
        var updateTodoDto = new UpdateTodoDto();
        updateTodoDto.setWorkName("Write UT");
        updateTodoDto.setStartDate("2022-05-19T08:00:00");
        updateTodoDto.setEndDate("2022-05-21T00:00:00");
        updateTodoDto.setStatus("DOING");
        return updateTodoDto;
    }

    public static PagingDto buildPagingDto() {
        return new PagingDto();
    }

}
