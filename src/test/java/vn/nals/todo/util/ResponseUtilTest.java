package vn.nals.todo.util;

import org.junit.jupiter.api.Test;
import vn.nals.todo.dto.PagingDto;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static vn.nals.todo.util.DTOBuilder.buildTodo;

class ResponseUtilTest {

    @Test
    void testWrapWithPaging() {
        var r = ResponseUtil.wrapWithPaging(List.of(buildTodo()), new PagingDto());
        assertTrue(r.containsKey("paging"));
        assertTrue(r.containsKey("data"));
    }
}