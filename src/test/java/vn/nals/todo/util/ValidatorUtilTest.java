package vn.nals.todo.util;

import org.junit.jupiter.api.Test;
import vn.nals.todo.exception.NalsValidationException;

import static org.junit.jupiter.api.Assertions.*;
import static vn.nals.todo.util.DTOBuilder.buildCreateTodoDto;

class ValidatorUtilTest {

    @Test
    void testValidate() {
        var dto = buildCreateTodoDto();
        assertDoesNotThrow(() -> ValidatorUtil.validate(dto));

        dto.setWorkName("");
        var e = assertThrows(NalsValidationException.class, () -> ValidatorUtil.validate(dto));
        assertEquals("workName: must not be blank", e.getMessage());
    }
}