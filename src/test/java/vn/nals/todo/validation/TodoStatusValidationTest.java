package vn.nals.todo.validation;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class TodoStatusValidationTest {

    @Test
    void testIsValid() {
        var validatorContext = mock(ConstraintValidatorContext.class);
        var validator = new TodoStatusValidation();

        assertFalse(validator.isValid(null, validatorContext));
        assertFalse(validator.isValid("", validatorContext));
        assertFalse(validator.isValid("planing", validatorContext));
        assertTrue(validator.isValid("PLANING", validatorContext));
        assertTrue(validator.isValid("DOING", validatorContext));
        assertTrue(validator.isValid("COMPLETED", validatorContext));
    }

}