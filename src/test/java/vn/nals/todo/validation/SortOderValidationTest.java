package vn.nals.todo.validation;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class SortOderValidationTest {

    @Test
    void testIsValid() {
        var validatorContext = mock(ConstraintValidatorContext.class);
        var validator = new SortOderValidation();

        assertTrue(validator.isValid(null, validatorContext));
        assertTrue(validator.isValid("", validatorContext));
        assertFalse(validator.isValid("abcd", validatorContext));
        assertTrue(validator.isValid("asc", validatorContext));
        assertTrue(validator.isValid("DESC", validatorContext));
    }


}