package vn.nals.todo.validation;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class DateTimeValidationTest {

    @Test
    void testIsValid() {
        var validatorContext = mock(ConstraintValidatorContext.class);
        var validator = new DateTimeValidation();

        assertFalse(validator.isValid(null, validatorContext));
        assertFalse(validator.isValid("2022-05-19", validatorContext));
        assertTrue(validator.isValid("2022-05-21T00:00:00", validatorContext));
    }

}