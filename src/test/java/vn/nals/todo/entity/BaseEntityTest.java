package vn.nals.todo.entity;

import org.junit.jupiter.api.Test;
import vn.nals.todo.util.Helper;

import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static vn.nals.todo.util.DTOBuilder.buildTodo;

class BaseEntityTest {

    @Test
    void testPrePersistAuditAndPreUpdateAudit() throws NoSuchMethodException {
        var todo = buildTodo();
        Method prePersistAudit = Helper.toPublicMethod(BaseEntity.class, "prePersistAudit");
        assertDoesNotThrow(() -> prePersistAudit.invoke(todo));
        Method preUpdateAudit = Helper.toPublicMethod(BaseEntity.class, "preUpdateAudit");
        assertDoesNotThrow(() -> preUpdateAudit.invoke(todo));
    }
}