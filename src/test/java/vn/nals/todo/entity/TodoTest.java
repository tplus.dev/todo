package vn.nals.todo.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static vn.nals.todo.util.DTOBuilder.buildTodo;

class TodoTest {

    @Test
    void testTodo() {
        var todo = buildTodo();
        assertEquals(0, todo.getStatus().getValue());
    }
}