package vn.nals.todo.dto;

import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;
import vn.nals.todo.entity.Todo;
import vn.nals.todo.exception.NalsValidationException;
import vn.nals.todo.util.Helper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;

class PagingDtoTest {

    @Test
    void testCheckValidSort() throws NoSuchMethodException {
        Method method = Helper.toPublicMethod(PagingDto.class,
                "checkValidSort",
                String.class,
                Class.class);

        var pagingDto = new PagingDto();
        assertDoesNotThrow(() -> method.invoke(pagingDto, "id", Todo.class));
        assertThrows(Exception.class, () -> method.invoke(pagingDto, "abc", Todo.class));
    }

    @Test
    void testGetDirectionFromString() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = Helper.toPublicMethod(PagingDto.class,
                "getDirectionFromString",
                String.class);
        
        var pagingDto = new PagingDto();
        assertEquals(Sort.Direction.ASC, method.invoke(pagingDto, "asc"));
        assertEquals(Sort.Direction.DESC, method.invoke(pagingDto, "desc"));
        assertEquals(Sort.Direction.ASC, method.invoke(pagingDto, "alalalala"));
    }
}