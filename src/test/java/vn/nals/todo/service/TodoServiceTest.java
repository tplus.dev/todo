package vn.nals.todo.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import vn.nals.todo.entity.Todo;
import vn.nals.todo.exception.NalsBusinessException;
import vn.nals.todo.exception.NalsValidationException;
import vn.nals.todo.repository.TodoRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static vn.nals.todo.util.DTOBuilder.*;

@ExtendWith(MockitoExtension.class)
class TodoServiceTest {

    @InjectMocks
    TodoService service;

    @Mock
    TodoRepository todoRepository;

    @Test
    void testGetWithPaging() {
        when(todoRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(List.of(buildTodo())));

        var pageTodo = service.getWithPaging(PageRequest.of(1,1));
        assertEquals(1, pageTodo.getTotalElements());
    }

    @Test
    void testSave() {
        when(todoRepository.save(any(Todo.class))).thenReturn(buildTodo());

        var createTodoDto = buildCreateTodoDto();
        assertNotNull(service.save(createTodoDto));

        createTodoDto.setStartDate("2222-05-21T00:00:00");
        var e = assertThrows(NalsBusinessException.class, () -> service.save(createTodoDto));
        assertEquals("end date cannot before start date", e.getMessage());
    }

    @Test
    void testUpdated() {
        when(todoRepository.saveAndFlush(any(Todo.class))).thenReturn(buildTodo());

        var updateTodoDto = buildUpdateTodoDto();
        var e = assertThrows(NalsValidationException.class, () -> service.update(null, updateTodoDto));
        assertEquals("not found todo with id: null", e.getMessage());

        when(todoRepository.findById(1L)).thenReturn(Optional.of(buildTodo()));
        assertNotNull(service.update(1L, updateTodoDto));

        updateTodoDto.setStartDate("2222-05-21T00:00:00");
        var ex = assertThrows(NalsBusinessException.class, () -> service.update(1L, updateTodoDto));
        assertEquals("end date cannot before start date", ex.getMessage());

    }

    @Test
    void testDelete() {
        doNothing().when(todoRepository).deleteById(1L);

        var e = assertThrows(NalsBusinessException.class, () -> service.delete(null));
        assertEquals("no entity with id null exists", e.getMessage());

        when(todoRepository.findById(1L)).thenReturn(Optional.of(buildTodo()));
        assertDoesNotThrow(() -> service.delete(1L));
    }

    @Test
    void testGetById() {
        var e = assertThrows(NalsValidationException.class, () -> service.getById(1L));
        assertEquals("not found todo with id: 1", e.getMessage());

        when(todoRepository.findById(1L)).thenReturn(Optional.of(buildTodo()));
        assertNotNull(service.getById(1L));
    }

}