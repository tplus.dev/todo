package vn.nals.todo.controller;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import vn.nals.todo.dto.PagingDto;
import vn.nals.todo.exception.NalsBusinessException;
import vn.nals.todo.exception.NalsValidationException;

import static org.junit.jupiter.api.Assertions.*;
import static vn.nals.todo.util.DTOBuilder.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TodoControllerIT {

    @Autowired
    TodoController controller;

    @Test
    @Order(1)
    void testGetAllWithDefaultPaging() {
        var res = controller.getAll(new PagingDto());
        var body = res.getBody();

        assertEquals(200, res.getStatusCodeValue());
        assertNotNull(body);
        assertTrue(body.containsKey("paging"));
        assertTrue(body.containsKey("data"));
    }

    @Test
    @Order(2)
    void testWithInvalidPaging() {
        var paging = buildPagingDto();
        paging.setSortBy("anyField");

        var e = assertThrows(NalsValidationException.class, () -> controller.getAll(paging));
        assertEquals("no field 'anyField' found for type 'Todo'", e.getMessage());
    }

    @Test
    @Order(3)
    void testCreateTodoOk() {
        var dto = buildCreateTodoDto();
        var res = controller.createTodo(dto);
        var body = res.getBody();

        assertEquals(200, res.getStatusCodeValue());
        assertNotNull(body);
        assertEquals("Write UT", body.getWorkName());
    }

    @Test
    @Order(4)
    void testCreateTodoWithStartDateGreaterThanEndDate() {
        var dto = buildCreateTodoDto();
        dto.setStartDate("2222-05-19T08:00:00");

        var e = assertThrows(NalsBusinessException.class, () -> controller.createTodo(dto));
        assertEquals("end date cannot before start date", e.getMessage());
    }

    @Test
    @Order(5)
    void testGetByIdOk() {
        var res = controller.getById(3L);
        var body = res.getBody();

        assertEquals(200, res.getStatusCodeValue());
        assertNotNull(body);
        assertEquals("Play game with friends", body.getWorkName());
    }

    @Test
    @Order(6)
    void testGetByIdButNotFound() {
        var e = assertThrows(NalsValidationException.class, () -> controller.getById(0L));
        assertEquals("not found todo with id: 0", e.getMessage());
    }

    @Test
    @Order(7)
    void testUpdateTodoOk() {
        var dto = buildUpdateTodoDto();
        var res = controller.updateTodo(2L, dto);
        var body = res.getBody();

        assertEquals(200, res.getStatusCodeValue());
        assertNotNull(body);
        assertEquals("Write UT", body.getWorkName());
    }

    @Test
    @Order(8)
    void testUpdateTodoButError() {
        var dto = buildUpdateTodoDto();
        dto.setStartDate("2222-05-19T08:00:00");

        var e = assertThrows(NalsBusinessException.class, () -> controller.updateTodo(2L, dto));
        assertEquals("end date cannot before start date", e.getMessage());
    }


    @Test
    @Order(9)
    void testDeleteTodoOk() {
        var res = controller.deleteTodo(4L);
        assertEquals(200, res.getStatusCodeValue());
    }

    @Test
    @Order(10)
    void testDeleteTodoButNotFound() {
        var e = assertThrows(NalsBusinessException.class, () -> controller.deleteTodo(0L));
        assertEquals("no entity with id 0 exists", e.getMessage());
    }

}