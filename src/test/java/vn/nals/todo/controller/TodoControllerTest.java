package vn.nals.todo.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import vn.nals.todo.dto.CreateTodoDto;
import vn.nals.todo.dto.PagingDto;
import vn.nals.todo.dto.UpdateTodoDto;
import vn.nals.todo.service.TodoService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static vn.nals.todo.util.DTOBuilder.*;

@ExtendWith(MockitoExtension.class)
class TodoControllerTest {

    @InjectMocks
    TodoController controller;

    @Mock
    TodoService todoService;

    @Test
    void testGetAllWithPaging() {
        when(todoService.getWithPaging(any(Pageable.class))).thenReturn(new PageImpl<>(List.of(buildTodo())));

        var pagingDto = buildPagingDto();
        var res = controller.getAll(pagingDto);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertNotNull(res.getBody());
    }

    @Test
    void testCreateNewTodo() {
        when(todoService.save(any(CreateTodoDto.class))).thenReturn(buildTodo());

        var createTodoDto = buildCreateTodoDto();
        var res = controller.createTodo(createTodoDto);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertNotNull(res.getBody());
    }

    @Test
    void testGetById() {
        when(todoService.getById(1L)).thenReturn(buildTodo());

        var res = controller.getById(1L);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertNotNull(res.getBody());
    }

    @Test
    void testUpdateTodo() {
        when(todoService.update(eq(1L), any(UpdateTodoDto.class))).thenReturn(buildTodo());

        var updateTodoDto = buildUpdateTodoDto();
        var res = controller.updateTodo(1L, updateTodoDto);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertNotNull(res.getBody());
    }

    @Test
    void testDeleteTodo() {
        doNothing().when(todoService).delete(1L);

        var res = controller.deleteTodo(1L);
        assertEquals(HttpStatus.OK, res.getStatusCode());
    }

}