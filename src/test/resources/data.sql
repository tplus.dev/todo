DROP TABLE IF EXISTS todo;
CREATE TABLE todo (
    id         bigint(20) NOT NULL,
    created_at datetime(6)  DEFAULT NULL,
    created_by varchar(255) DEFAULT NULL,
    deleted    bit(1)       DEFAULT NULL,
    updated_at datetime(6)  DEFAULT NULL,
    updated_by varchar(255) DEFAULT NULL,
    end_date   datetime(6)  DEFAULT NULL,
    start_date datetime(6)  DEFAULT NULL,
    status     int(11)      DEFAULT NULL,
    work_name  varchar(255) DEFAULT NULL
);

INSERT INTO todo
VALUES (1, '2022-05-19 07:59:25.007100', 'vn.nals.todo.service.TodoService', '0', '2022-05-19 08:06:06.686418',
        'vn.nals.todo.service.TodoService', '2022-05-21 00:00:00.000000', '2022-05-18 12:00:00.000000', 1,
        'Do NALS test'),
       (2, '2022-05-19 08:00:28.317098', 'vn.nals.todo.service.TodoService', '0', '2022-05-19 08:00:28.317098',
        'vn.nals.todo.service.TodoService', '2022-05-19 11:00:00.000000', '2022-05-19 10:00:00.000000', 0,
        'Talk with HR Nals'),
       (3, '2022-05-19 08:00:56.279539', 'vn.nals.todo.service.TodoService', '0', '2022-05-19 08:00:56.280078',
        'vn.nals.todo.service.TodoService', '2022-05-19 22:00:00.000000', '2022-05-19 20:00:00.000000', 0,
        'Play game with friends'),
       (4, '2022-05-19 08:02:03.965218', 'vn.nals.todo.service.TodoService', '0', '2022-05-19 08:05:33.030228',
        'vn.nals.todo.service.TodoService', '2022-05-18 19:00:00.000000', '2022-05-18 18:00:00.000000', 2,
        'Shoping (buy food, drink,...)'),
       (5, '2022-05-19 08:02:10.052581', 'vn.nals.todo.service.TodoService', '0', '2022-05-19 08:02:10.053128',
        'vn.nals.todo.service.TodoService', '2022-05-19 19:00:00.000000', '2022-05-19 18:00:00.000000', 0,
        'Shoping (buy food, drink,...)'),
       (6, '2022-05-19 08:02:17.578761', 'vn.nals.todo.service.TodoService', '0', '2022-05-19 08:02:17.578761',
        'vn.nals.todo.service.TodoService', '2022-05-20 19:00:00.000000', '2022-05-20 18:00:00.000000', 0,
        'Shoping (buy food, drink,...)');
